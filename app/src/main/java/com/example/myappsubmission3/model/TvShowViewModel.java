package com.example.myappsubmission3.model;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class TvShowViewModel extends ViewModel {

    private static final String API_KEY = "e97b7ab2dfdee16584561d1c3d1e87de";
    private MutableLiveData<ArrayList<TvShowItems>> listTvShow = new MutableLiveData<>();

    public void setTvShow(String search){
        AsyncHttpClient client = new AsyncHttpClient();
        final ArrayList<TvShowItems> listItems = new ArrayList<>();

        String url = "";

        if(!search.isEmpty()){
            url = "https://api.themoviedb.org/3/search/tv?api_key="+API_KEY+"&language=en-US&query="+search;
        }else{
            url = "https://api.themoviedb.org/3/discover/tv?api_key="+API_KEY+"&language=en-US";
        }

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray list = responseObject.getJSONArray("results");

                    for (int i = 0; i < list.length(); i++ ){
                        JSONObject tvShow = list.getJSONObject(i);
                        TvShowItems tvShowItems = new TvShowItems();
                        tvShowItems.setId(tvShow.getInt("id"));
                        tvShowItems.setName(tvShow.getString("original_name"));
                        tvShowItems.setDescription(tvShow.getString("overview"));
                        tvShowItems.setRelease_date(tvShow.getString("first_air_date"));
                        tvShowItems.setRating(tvShow.getString("vote_average"));
                        tvShowItems.setOriginal_language(tvShow.getString("original_language"));
                        tvShowItems.setPopularity(tvShow.getString("popularity"));
                        tvShowItems.setPhoto(tvShow.getString("poster_path"));

                        listItems.add(tvShowItems);
                    }
                    listTvShow.postValue(listItems);
                } catch (JSONException e) {
                    Log.d("Exception", e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("onFailure", error.getMessage());
            }
        });

    }

    public LiveData<ArrayList<TvShowItems>> getTvShow(){
        return listTvShow;
    }
}
