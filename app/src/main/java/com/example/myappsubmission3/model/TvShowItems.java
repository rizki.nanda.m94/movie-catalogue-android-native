package com.example.myappsubmission3.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

@SuppressLint("ParcelCreator")
public class TvShowItems implements Parcelable {

    private int id;
    private String photo;
    private String name;
    private String description;
    private String rating;
    private String release_date;
    private String original_language;
    private String popularity;

    public TvShowItems() {
    }

    public TvShowItems(int id, String photo, String name, String description, String rating, String release_date, String original_language, String popularity) {
        this.id = id;
        this.photo = photo;
        this.name = name;
        this.description = description;
        this.rating = rating;
        this.release_date = release_date;
        this.original_language = original_language;
        this.popularity = popularity;
    }

    protected TvShowItems(Parcel in) {
        this.id = in.readInt();
        this.photo = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.rating = in.readString();
        this.release_date = in.readString();
        this.original_language = in.readString();
        this.popularity = in.readString();
    }

    public static final Parcelable.Creator<TvShowItems> CREATOR = new Parcelable.Creator<TvShowItems>() {
        @Override
        public TvShowItems createFromParcel(Parcel in) {
            return new TvShowItems(in);
        }

        @Override
        public TvShowItems[] newArray(int size) {
            return new TvShowItems[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeInt(id);
        dest.writeString(photo);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(rating);
        dest.writeString(release_date);
        dest.writeString(original_language);
        dest.writeString(popularity);
    }
}
