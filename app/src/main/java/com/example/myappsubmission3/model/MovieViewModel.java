package com.example.myappsubmission3.model;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class MovieViewModel extends ViewModel {
    private static final String API_KEY = "e97b7ab2dfdee16584561d1c3d1e87de";
    private MutableLiveData<ArrayList<MovieItems>> listMovie = new MutableLiveData<>();

    public void setMovie(String search){
        AsyncHttpClient client = new AsyncHttpClient();
        final ArrayList<MovieItems> listItems = new ArrayList<>();

        String url = "";

        if(!search.isEmpty()){
            url = "https://api.themoviedb.org/3/search/movie?api_key="+API_KEY+"&language=en-US&query="+search;
        }else{
            url = "https://api.themoviedb.org/3/discover/movie?api_key="+API_KEY+"&language=en-US";
        }

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray list = responseObject.getJSONArray("results");

                    for (int i = 0; i < list.length(); i++ ){
                        JSONObject movie = list.getJSONObject(i);
                        MovieItems movieItems = new MovieItems();
                        movieItems.setId(movie.getInt("id"));
                        movieItems.setName(movie.getString("original_title"));
                        movieItems.setDescription(movie.getString("overview"));

                        String isReleaseDate = movie.optString("release_date");

                        if(isReleaseDate != ""){
                            movieItems.setRelease_date(movie.getString("release_date"));
                        }
                        movieItems.setRating(movie.getString("vote_average"));
                        movieItems.setOriginal_language(movie.getString("original_language"));
                        movieItems.setPopularity(movie.getString("popularity"));
                        movieItems.setPhoto(movie.getString("poster_path"));

                        listItems.add(movieItems);
                    }
                    listMovie.postValue(listItems);
                } catch (JSONException e) {
                    Log.d("Exception", e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("onFailure", error.getMessage());
            }
        });
    }

    public LiveData<ArrayList<MovieItems>> getMovie(){
        return listMovie;
    }
}
