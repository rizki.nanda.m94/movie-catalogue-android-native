package com.example.myappsubmission3.helper;

import android.database.Cursor;

import com.example.myappsubmission3.db.DatabaseContract;
import com.example.myappsubmission3.model.MovieItems;
import com.example.myappsubmission3.model.TvShowItems;

import java.util.ArrayList;

public class MappingHelper {
    public static ArrayList<MovieItems> mapCursorToArrayList(Cursor moviesCursor){
        ArrayList<MovieItems> movieList = new ArrayList<>();

        while (moviesCursor.moveToNext()){
            int id = moviesCursor.getInt(moviesCursor.getColumnIndexOrThrow(DatabaseContract.MovieColumns._ID));
            String photo = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DatabaseContract.MovieColumns.PHOTO));
            String title = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DatabaseContract.MovieColumns.TITLE));
            String description = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DatabaseContract.MovieColumns.DESCRIPTION));
            String original_language = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DatabaseContract.MovieColumns.ORIGINAL_LANGUAGE));
            String popularity = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DatabaseContract.MovieColumns.POPULARITY));
            String rating = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DatabaseContract.MovieColumns.RATING));
            String release_date = moviesCursor.getString(moviesCursor.getColumnIndexOrThrow(DatabaseContract.MovieColumns.RELEASE_DATE));
            movieList.add(new MovieItems(id, photo, title, description, original_language, popularity, rating, release_date));
        }

        return movieList;
    }

    public static ArrayList<TvShowItems> mapCursorToArrayListTvShow(Cursor TvShowCursor){
        ArrayList<TvShowItems> tvList = new ArrayList<>();

        while (TvShowCursor.moveToNext()){
            int id = TvShowCursor.getInt(TvShowCursor.getColumnIndexOrThrow(DatabaseContract.TvShowColumns._ID));
            String photo = TvShowCursor.getString(TvShowCursor.getColumnIndexOrThrow(DatabaseContract.TvShowColumns.PHOTO));
            String title = TvShowCursor.getString(TvShowCursor.getColumnIndexOrThrow(DatabaseContract.TvShowColumns.TITLE));
            String description = TvShowCursor.getString(TvShowCursor.getColumnIndexOrThrow(DatabaseContract.TvShowColumns.DESCRIPTION));
            String original_language = TvShowCursor.getString(TvShowCursor.getColumnIndexOrThrow(DatabaseContract.TvShowColumns.ORIGINAL_LANGUAGE));
            String popularity = TvShowCursor.getString(TvShowCursor.getColumnIndexOrThrow(DatabaseContract.TvShowColumns.POPULARITY));
            String rating = TvShowCursor.getString(TvShowCursor.getColumnIndexOrThrow(DatabaseContract.TvShowColumns.RATING));
            String release_date = TvShowCursor.getString(TvShowCursor.getColumnIndexOrThrow(DatabaseContract.TvShowColumns.RELEASE_DATE));
            tvList.add(new TvShowItems(id, photo, title, description, original_language, popularity, rating, release_date));
        }

        return tvList;
    }


}
