package com.example.myappsubmission3;


import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;

import androidx.fragment.app.Fragment;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.myappsubmission3.model.MovieItems;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingReminderFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener {

    private RequestQueue mRequestQueue;
    List<MovieItems> mNotifList;
    AlarmReceiver mMovieDailyReceiver = new AlarmReceiver();
    AlarmReleaseReceiver mMovieReleaseReceiver = new AlarmReleaseReceiver();
    SwitchPreference mSwitchReminder;
    SwitchPreference mSwitchReleaseToday;

    private String DEFAULT_VALUE = "-";

    @Override
    public void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        mNotifList = new ArrayList<>();
        mRequestQueue = Volley.newRequestQueue(getActivity());

        init();
    }

    private void init(){
        mSwitchReminder = (SwitchPreference) findPreference(getString(R.string.daily_switch));
        mSwitchReminder.setOnPreferenceChangeListener(this);

        mSwitchReleaseToday = (SwitchPreference) findPreference(getString(R.string.upcoming_switch));
        mSwitchReleaseToday.setOnPreferenceChangeListener(this);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String key = preference.getKey();
        boolean value = (boolean) newValue;
        if(key.equals(getString(R.string.daily_switch))){
            if(value){
                mMovieDailyReceiver.setOneTimeAlarm(getActivity());
            }else{
                mMovieDailyReceiver.cancelAlarm(getActivity());
            }
        }

        if(key.equals(getString(R.string.upcoming_switch))){
            if(value){
                mMovieReleaseReceiver.setReleaseMovieAlarm(getActivity());
            }else{
                mMovieReleaseReceiver.cancelAlarm(getActivity());
            }
        }

        return true;
    }
}
