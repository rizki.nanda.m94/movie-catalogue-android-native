package com.example.myappsubmission3;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AlarmReleaseReceiver extends BroadcastReceiver {

    private static int mNotifId = 2000;

        private static PendingIntent getPendingIntent(Context context) {
            int ALARM_RELEASE_ID = 1011;
            Intent intent = new Intent(context, AlarmReleaseReceiver.class);
            return PendingIntent.getBroadcast(context, ALARM_RELEASE_ID, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        }

        public void setReleaseMovieAlarm(Context context){

            AlarmManager alarmReleaseMovieManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            Intent intent = new Intent(context, AlarmReleaseReceiver.class);

            Calendar calendar = Calendar.getInstance();
            // setiap hari jam 8 pagi
            calendar.set(Calendar.HOUR_OF_DAY, 8);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            Calendar cur = Calendar.getInstance();

        if (cur.after(calendar)) {
            calendar.add(Calendar.DATE, 1);
        }

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        if (alarmReleaseMovieManager != null) {
            alarmReleaseMovieManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }

        Toast.makeText(context, "Upcoming Notif ON", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.d("AlarmManager","Alarm Triggered");

        setReleaseMovieAlarm(context);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        final String todaysDate = dateFormat.format(date);

        String url = "https://api.themoviedb.org/3/discover/movie?api_key=e97b7ab2dfdee16584561d1c3d1e87de&primary_release_date.gte="+todaysDate+"&primary_release_date.lte="+todaysDate;

        RequestQueue requestQueue = Volley.newRequestQueue(context);;

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET, url, null, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try{
                    Log.d("AlarmManager Response","sukses");
                    JSONArray jsonArray = response.getJSONArray("results");

                    int idNotification = 0;
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                    Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_local_movies_black);
                    Intent intent = new Intent(context, AlarmReleaseReceiver.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(context, mNotifId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder mBuilder = null;
                    NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
                    int maxMovies = 6;
                    if(maxMovies > jsonArray.length())
                        maxMovies = jsonArray.length();
                    for(int i = 0; i < maxMovies; i++){
                        inboxStyle.addLine(jsonArray.getJSONObject(i).getString("title"));
                    }

                    mBuilder = new NotificationCompat.Builder(context, "11011")
                            .setContentTitle( jsonArray.length() + " " + context.getResources().getString(R.string.content_release_title))
                            .setSmallIcon(R.drawable.ic_local_movies_black)
                            .setLargeIcon(largeIcon)
                            .setGroup( "group_key_new_release")
                            .setGroupSummary(true)
                            .setContentIntent(pendingIntent)
                            .setStyle(inboxStyle)
                            .setAutoCancel(true);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationChannel channel = new NotificationChannel("11011",
                                "channel",
                                NotificationManager.IMPORTANCE_DEFAULT);

                        mBuilder.setChannelId("11011");

                        if (mNotificationManager != null) {
                            mNotificationManager.createNotificationChannel(channel);
                        }
                    }

                    Notification notification = mBuilder.build();
                    if (mNotificationManager != null) {
                        mNotificationManager.notify(idNotification, notification);
                    }

                }catch (JSONException e){
                    Log.d("AlarmManager Response","gagal");
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("AlarmManager Response","error volley");

                error.printStackTrace();

                Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(request);

    }

    public void cancelAlarm(Context context){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(getPendingIntent(context));
        Toast.makeText(context, "Upcoming Notif OFF", Toast.LENGTH_SHORT).show();
    }

}
