package com.example.myappsubmission3;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.myappsubmission3.db.MovieHelper;
import com.example.myappsubmission3.model.MovieItems;

import static android.provider.BaseColumns._ID;
import static com.example.myappsubmission3.db.DatabaseContract.MovieColumns.DESCRIPTION;
import static com.example.myappsubmission3.db.DatabaseContract.MovieColumns.ORIGINAL_LANGUAGE;
import static com.example.myappsubmission3.db.DatabaseContract.MovieColumns.POPULARITY;
import static com.example.myappsubmission3.db.DatabaseContract.MovieColumns.RATING;
import static com.example.myappsubmission3.db.DatabaseContract.MovieColumns.RELEASE_DATE;
import static com.example.myappsubmission3.db.DatabaseContract.MovieColumns.TITLE;
import static com.example.myappsubmission3.db.DatabaseContract.MovieColumns.PHOTO;

import com.squareup.picasso.Picasso;

public class MovieDetailActivity extends AppCompatActivity implements View.OnClickListener{

    private MovieItems movie;

    private int position;
    private MovieHelper movieHelper;

    public static final String EXTRA_IMG = "extra_img";
    public static final String EXTRA_MOVIE = "extra_movie";
    public static final String EXTRA_POSITION = "extra_position";
    public static final int RESULT_ADD = 101;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        Toolbar toolbar = findViewById(R.id.toolbar_detail);
        TextView tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        ImageButton favButton = findViewById(R.id.btn_toolbar_profile);

        setSupportActionBar(toolbar);
        tv_toolbar_title.setText(R.string.information_detail);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        ImageView ivPhoto = findViewById(R.id.iv_image);
        TextView tvTitle = findViewById(R.id.text_title);
        TextView tvDescription = findViewById(R.id.text_overview);
        TextView tvReleaseDate = findViewById(R.id.text_release_date);
        TextView tvOriginalLanguage = findViewById(R.id.text_language);
        TextView tvPopularity = findViewById(R.id.text_popularity);
        TextView tvRating = findViewById(R.id.text_rating);

        movieHelper = MovieHelper.getInstance(getApplicationContext());

        movie = getIntent().getParcelableExtra(EXTRA_MOVIE);

        String photo = movie.getPhoto();
        String title = movie.getName();
        String description = movie.getDescription();
        String release = movie.getRelease_date();
        String language = movie.getOriginal_language();
        String popularity = movie.getPopularity();
        String rating = movie.getRating();

        String photo_url_str = "https://image.tmdb.org/t/p/w342/"+photo;

        Picasso.get().load(photo_url_str).into(ivPhoto);
        tvTitle.setText(title);
        tvDescription.setText(description);
        tvReleaseDate.setText(release);
        tvOriginalLanguage.setText(language);
        tvPopularity.setText(popularity);
        tvRating.setText(rating);

        favButton.setOnClickListener(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_toolbar_profile){
            int id = movie.getId();
            String photo = movie.getPhoto();
            String title = movie.getName();
            String description = movie.getDescription();
            String release = movie.getRelease_date();
            String language = movie.getOriginal_language();
            String popularity = movie.getPopularity();
            String rating = movie.getRating();

            movie.setId(id);
            movie.setName(title);
            movie.setDescription(description);
            movie.setRelease_date(release);
            movie.setOriginal_language(language);
            movie.setPopularity(popularity);
            movie.setRating(rating);
            movie.setPhoto(photo);

            Intent intent = new Intent();
            intent.putExtra(EXTRA_MOVIE, movie);
            intent.putExtra(EXTRA_POSITION, position);

            ContentValues values = new ContentValues();
            values.put(_ID, id);
            values.put(PHOTO, photo);
            values.put(TITLE, title);
            values.put(DESCRIPTION, description);
            values.put(ORIGINAL_LANGUAGE, language);
            values.put(POPULARITY, popularity);
            values.put(RATING, rating);
            values.put(RELEASE_DATE, release);

            movieHelper.open();

            long result = movieHelper.insert(values);
            if(result > 0){
                movie.setId((int) result);
                setResult(RESULT_ADD, intent);
                finish();
                Toast.makeText(MovieDetailActivity.this, "Berhasil menambahkan ke favorit", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(MovieDetailActivity.this, "Movie sudah ditambahkan ke favorit", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
