package com.example.myappsubmission3.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myappsubmission3.MovieDetailActivity;
import com.example.myappsubmission3.R;
import com.example.myappsubmission3.adapter.ListMovieAdapter;
import com.example.myappsubmission3.model.MovieItems;
import com.example.myappsubmission3.model.MovieViewModel;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MoviesFragment extends Fragment {

    private ProgressBar progressBar;

    private MovieViewModel movieViewModel;

    private static final String ARG_SECTION_NUMBER = "section_number";

    private ArrayList<MovieItems> list = new ArrayList<>();

    EditText edtCariMovie;
    Button btnCari;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.rv_movies);
        progressBar = view.findViewById(R.id.progressBar);

        final ListMovieAdapter recyclerAdapter = new ListMovieAdapter();
        recyclerAdapter.notifyDataSetChanged();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(recyclerAdapter);

        movieViewModel = new ViewModelProvider(this, new ViewModelProvider.NewInstanceFactory()).get(MovieViewModel.class);

        edtCariMovie = view.findViewById(R.id.search_movies);

        movieViewModel.setMovie("");
        btnCari = view.findViewById(R.id.btn_cari);

        btnCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text_cari =  edtCariMovie.getText().toString();
                movieViewModel.setMovie(text_cari);
            }
        });

        movieViewModel.getMovie().observe(this, new Observer<ArrayList<MovieItems>>() {

            @Override
            public void onChanged(ArrayList<MovieItems> movieItems) {
                if(movieItems != null){
                    recyclerAdapter.setData(movieItems);
                    showLoading(false);
                }
            }
        });

        recyclerAdapter.setOnItemClickCallback(new ListMovieAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(MovieItems data) {
                addItem(data);
            }
        });

        return view;
    }

    private void showLoading(boolean state){
        if(state){
            progressBar.setVisibility(View.VISIBLE);
        }else {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void addItem(MovieItems data) {
        list = new ArrayList<>();
        MovieItems movie = new MovieItems();
        movie.setId(data.getId());
        movie.setPhoto(data.getPhoto());
        movie.setName(data.getName());
        movie.setDescription(data.getDescription());
        movie.setRelease_date(data.getRelease_date());
        movie.setRating(data.getRating());
        movie.setOriginal_language(data.getOriginal_language());
        movie.setPopularity(data.getPopularity());
        list.add(movie);

        Intent moveWithObjectIntent = new Intent(getActivity(), MovieDetailActivity.class);
        moveWithObjectIntent.putExtra(MovieDetailActivity.EXTRA_MOVIE, movie);
        moveWithObjectIntent.putExtra(MovieDetailActivity.EXTRA_IMG, movie.getPhoto());
        startActivity(moveWithObjectIntent);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle saveInstanceState){
        super.onViewCreated(view, saveInstanceState);
        int index=1;

        if(getArguments() != null){
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
    }

}
