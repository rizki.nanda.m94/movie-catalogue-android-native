package com.example.myappsubmission3.fragment.favorite;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myappsubmission3.MovieDetailActivity;
import com.example.myappsubmission3.R;
import com.example.myappsubmission3.adapter.favorite.ListFavMovieAdapter;
import com.example.myappsubmission3.db.MovieHelper;
import com.example.myappsubmission3.helper.MappingHelper;
import com.example.myappsubmission3.model.MovieItems;
import com.google.android.material.snackbar.Snackbar;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class FavMoviesFragment extends Fragment implements LoadMoviesCallback{

    ProgressBar progressBar;
    private MovieHelper movieHelper;
    private ListFavMovieAdapter adapter;
    private RecyclerView rvMovieFav;

    private ArrayList<MovieItems> list = new ArrayList<>();

    private static final String EXTRA_STATE = "EXTRA_STATE";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fav_movies, container, false);

        rvMovieFav = v.findViewById(R.id.rv_movies_favorite);
        progressBar = v.findViewById(R.id.progressBar);

        rvMovieFav.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMovieFav.setHasFixedSize(true);

        adapter = new ListFavMovieAdapter(getActivity());
        rvMovieFav.setAdapter(adapter);

        movieHelper = MovieHelper.getInstance(getActivity());
        movieHelper.open();
        
        adapter.setOnItemClickCallback(new ListFavMovieAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(MovieItems data) {
                addItem(data);
            }
        });

        if(savedInstanceState == null){
            //proses ambil data
            new LoadMoviesAsync(movieHelper, this).execute();
        }else{
            ArrayList<MovieItems> list = savedInstanceState.getParcelableArrayList(EXTRA_STATE);
            if(list != null){
                adapter.setListMovie(list);
            }
        }

        return v;
    }

    private void addItem(MovieItems data) {
        list = new ArrayList<>();
        MovieItems movie = new MovieItems();
        movie.setId(data.getId());
        movie.setPhoto(data.getPhoto());
        movie.setName(data.getName());
        movie.setDescription(data.getDescription());
        movie.setRelease_date(data.getRelease_date());
        movie.setRating(data.getRating());
        movie.setOriginal_language(data.getOriginal_language());
        movie.setPopularity(data.getPopularity());
        list.add(movie);

        Intent moveWithObjectIntent = new Intent(getActivity(), MovieDetailActivity.class);
        moveWithObjectIntent.putExtra(MovieDetailActivity.EXTRA_MOVIE, movie);
        moveWithObjectIntent.putExtra(MovieDetailActivity.EXTRA_IMG, movie.getPhoto());
        startActivity(moveWithObjectIntent);
    }

    @Override
    public void preExecute() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void postExecute(ArrayList<MovieItems> movie) {
        progressBar.setVisibility(View.INVISIBLE);
        if(movie.size() > 0){
            adapter.setListMovie(movie);
        }else{
            adapter.setListMovie(new ArrayList<MovieItems>());
            showSnackBarMessage("Tidak ada data saat ini");
        }
    }

    private void showSnackBarMessage(String message) {
        Snackbar.make(rvMovieFav, message, Snackbar.LENGTH_SHORT).show();
    }

    private static class LoadMoviesAsync extends AsyncTask<Void, Void, ArrayList<MovieItems>>{

        private final WeakReference<MovieHelper> weakMovieHelper;
        private final WeakReference<LoadMoviesCallback> weakCallback;

        private LoadMoviesAsync(MovieHelper movieHelper, LoadMoviesCallback callback){
            weakMovieHelper = new WeakReference<>(movieHelper);
            weakCallback = new WeakReference<>(callback);
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            weakCallback.get().preExecute();
        }

        @Override
        protected ArrayList<MovieItems> doInBackground(Void... voids) {
            Cursor dataCursor = weakMovieHelper.get().queryAll();
            return MappingHelper.mapCursorToArrayList(dataCursor);
        }

        @Override
        protected void onPostExecute(ArrayList<MovieItems> movies){
            super.onPostExecute(movies);

            weakCallback.get().postExecute(movies);
        }
    }
}

interface LoadMoviesCallback{
    void preExecute();
    void postExecute(ArrayList<MovieItems> movieItems);
}
