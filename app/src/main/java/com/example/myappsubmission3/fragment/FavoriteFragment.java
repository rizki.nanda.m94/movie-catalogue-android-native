package com.example.myappsubmission3.fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myappsubmission3.R;
import com.example.myappsubmission3.adapter.SectionsFavPagerAdapter;
import com.example.myappsubmission3.adapter.SectionsPagerAdapter;
import com.google.android.material.tabs.TabLayout;


public class FavoriteFragment extends Fragment {

    private FragmentActivity myContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_favorite, container, false);

        FragmentManager fragmentManagerFav = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManagerFav.beginTransaction();
        fragmentTransaction.commit();

        SectionsFavPagerAdapter sectionsPagerAdapter = new SectionsFavPagerAdapter(this, getChildFragmentManager());
        ViewPager viewPager = v.findViewById(R.id.view_pager_favorite);
        viewPager.setAdapter(sectionsPagerAdapter);

        TabLayout tabs = v.findViewById(R.id.tabs_favorite);
        tabs.setupWithViewPager(viewPager);

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }

    }
