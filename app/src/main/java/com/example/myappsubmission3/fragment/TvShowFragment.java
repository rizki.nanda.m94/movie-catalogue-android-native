package com.example.myappsubmission3.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myappsubmission3.MovieDetailActivity;
import com.example.myappsubmission3.R;
import com.example.myappsubmission3.TvShowDetailActivity;
import com.example.myappsubmission3.adapter.ListTvShowAdapter;
import com.example.myappsubmission3.model.MovieItems;
import com.example.myappsubmission3.model.TvShowItems;
import com.example.myappsubmission3.model.TvShowViewModel;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class TvShowFragment extends Fragment {

    private ProgressBar progressBar;

    private TvShowViewModel tvShowViewModel;

    private ArrayList<TvShowItems> list = new ArrayList<>();

    private static final String ARG_SECTION_NUMBER = "section_number";

    EditText editCariTv;
    Button btnCari;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tv_show, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.rv_tv_show);

        progressBar = view.findViewById(R.id.progressBar);

        final ListTvShowAdapter listTvShowAdapter = new ListTvShowAdapter();
        listTvShowAdapter.notifyDataSetChanged();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(listTvShowAdapter);

        tvShowViewModel = new ViewModelProvider(this, new ViewModelProvider.NewInstanceFactory()).get(TvShowViewModel.class);

        editCariTv = view.findViewById(R.id.search_tvShow);

        tvShowViewModel.setTvShow("");
        btnCari = view.findViewById(R.id.btn_cari);

        btnCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text_cari =  editCariTv.getText().toString();
                tvShowViewModel.setTvShow(text_cari);
            }
        });

        tvShowViewModel.getTvShow().observe(this, new Observer<ArrayList<TvShowItems>>() {

            @Override
            public void onChanged(ArrayList<TvShowItems> tvShowItems) {
                if(tvShowItems != null){
                    listTvShowAdapter.setData(tvShowItems);
                    showLoading(false);
                }
            }
        });

        listTvShowAdapter.setOnItemClickCallback(new ListTvShowAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(TvShowItems data) {
                addItem(data);
            }
        });

        return view;
    }

    private void showLoading(boolean state){
        if(state){
            progressBar.setVisibility(View.VISIBLE);
        }else {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void addItem(TvShowItems data) {
        TvShowItems tv = new TvShowItems();
        tv.setId(data.getId());
        tv.setPhoto(data.getPhoto());
        tv.setName(data.getName());
        tv.setDescription(data.getDescription());
        tv.setRelease_date(data.getRelease_date());
        tv.setRating(data.getRating());
        tv.setOriginal_language(data.getOriginal_language());
        tv.setPopularity(data.getPopularity());
        list.add(tv);

        Intent moveWithObjectIntent = new Intent(getActivity(), TvShowDetailActivity.class);
        moveWithObjectIntent.putExtra(TvShowDetailActivity.EXTRA_TV, tv);
        moveWithObjectIntent.putExtra(TvShowDetailActivity.EXTRA_IMG, tv.getPhoto());
        startActivity(moveWithObjectIntent);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle saveInstanceState){
        super.onViewCreated(view, saveInstanceState);
        int index=1;

        if(getArguments() != null){
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
    }
}
