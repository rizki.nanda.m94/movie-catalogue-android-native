package com.example.myappsubmission3.fragment.favorite;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myappsubmission3.R;
import com.example.myappsubmission3.TvShowDetailActivity;
import com.example.myappsubmission3.adapter.favorite.ListFavTvShowAdapter;
import com.example.myappsubmission3.db.TvShowHelper;
import com.example.myappsubmission3.helper.MappingHelper;
import com.example.myappsubmission3.model.TvShowItems;
import com.google.android.material.snackbar.Snackbar;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class FavTvShowFragment extends Fragment implements LoadTvCallback {

    ProgressBar progressBar;
    private TvShowHelper tvShowHelper;
    private ListFavTvShowAdapter adapter;
    private RecyclerView rvTvShowFav;

    private ArrayList<TvShowItems> list = new ArrayList<>();

    private static final String EXTRA_STATE = "EXTRA_STATE";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fav_tv, container, false);

        rvTvShowFav = v.findViewById(R.id.rv_tvshow_favorite);
        progressBar = v.findViewById(R.id.progressBar);

        rvTvShowFav.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTvShowFav.setHasFixedSize(true);

        adapter = new ListFavTvShowAdapter(getActivity());
        rvTvShowFav.setAdapter(adapter);

        tvShowHelper = TvShowHelper.getInstance(getActivity());
        tvShowHelper.open();

        adapter.setOnItemClickCallback(new ListFavTvShowAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(TvShowItems data) {
                addItem(data);
            }
        });

        if(savedInstanceState == null){
            //proses ambil data
            new LoadTvAsync(tvShowHelper,  this).execute();
        }else{
            ArrayList<TvShowItems> list = savedInstanceState.getParcelableArrayList(EXTRA_STATE);
            if(list != null){
                adapter.setListTvShow(list);
            }
        }

        return v;
    }

    private void addItem(TvShowItems data) {
        TvShowItems tv = new TvShowItems();
        tv.setId(data.getId());
        tv.setPhoto(data.getPhoto());
        tv.setName(data.getName());
        tv.setDescription(data.getDescription());
        tv.setRelease_date(data.getRelease_date());
        tv.setRating(data.getRating());
        tv.setOriginal_language(data.getOriginal_language());
        tv.setPopularity(data.getPopularity());
        list.add(tv);

        Intent moveWithObjectIntent = new Intent(getActivity(), TvShowDetailActivity.class);
        moveWithObjectIntent.putExtra(TvShowDetailActivity.EXTRA_TV, tv);
        moveWithObjectIntent.putExtra(TvShowDetailActivity.EXTRA_IMG, tv.getPhoto());
        startActivity(moveWithObjectIntent);
    }

    @Override
    public void preExecute() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void postExecute(ArrayList<TvShowItems> tv) {
        progressBar.setVisibility(View.INVISIBLE);
        if(tv.size() > 0){
            adapter.setListTvShow(tv);
        }else{
            adapter.setListTvShow(new ArrayList<TvShowItems>());
            showSnackBarMessage("Tidak ada data saat ini");
        }
    }

    private void showSnackBarMessage(String message) {
        Snackbar.make(rvTvShowFav, message, Snackbar.LENGTH_SHORT).show();
    }


    private static class LoadTvAsync extends AsyncTask<Void, Void, ArrayList<TvShowItems>>{

        private final WeakReference<TvShowHelper> weakTvHelper;
        private final WeakReference<LoadTvCallback> weakCallback;

        private LoadTvAsync(TvShowHelper tvShowHelper, LoadTvCallback callback) {
            weakTvHelper = new WeakReference<>(tvShowHelper);
            weakCallback = new WeakReference<>(callback);
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            weakCallback.get().preExecute();
        }

        @Override
        protected ArrayList<TvShowItems> doInBackground(Void... voids) {
            Cursor dataCursor = weakTvHelper.get().queryAll();
            return MappingHelper.mapCursorToArrayListTvShow(dataCursor);
        }

        @Override
        protected void onPostExecute(ArrayList<TvShowItems> tv){
            super.onPostExecute(tv);

            weakCallback.get().postExecute(tv);
        }
    }

}

interface LoadTvCallback{
    void preExecute();
    void postExecute(ArrayList<TvShowItems> tv);
}
