package com.example.myappsubmission3;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class SettingReminderActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingReminderFragment())
                .commit();
    }

}
