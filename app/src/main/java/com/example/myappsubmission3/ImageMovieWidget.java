package com.example.myappsubmission3;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;
import android.widget.Toast;

/**
 * Implementation of App Widget functionality.
 */
public class ImageMovieWidget extends AppWidgetProvider {

    public static final String UPDATE_WIDGET = "com.example.myappsubmission3.UPDATE_WIDGET";
    private static final String TOAST_ACTION = "com.dicoding.myappsubmission3.TOAST_ACTION";
    public static final String EXTRA_ITEM = "com.dicoding.myappsubmission3.EXTRA_ITEM";

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        Intent intent = new Intent(context, StackWidgetService.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        intent.setAction(UPDATE_WIDGET);
        context.sendBroadcast(intent);

        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.image_movie_widget);
        views.setRemoteAdapter(R.id.stack_view, intent);
        views.setEmptyView(R.id.stack_view, R.id.empty_view);

        Intent toastIntent = new Intent(context, ImageMovieWidget.class);
        toastIntent.setAction(ImageMovieWidget.TOAST_ACTION);
        toastIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
        PendingIntent toastPendingIntent = PendingIntent.getBroadcast(context, 0, toastIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setPendingIntentTemplate(R.id.stack_view, toastPendingIntent);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent){
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

        super.onReceive(context,intent);
        if(intent.getAction() != null){
            if(intent.getAction().equals(TOAST_ACTION)){
                int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
                int viewIndex = intent.getIntExtra(EXTRA_ITEM, 0);
                Toast.makeText(context, "Touched view " + viewIndex, Toast.LENGTH_SHORT ).show();
            }
        }else if(intent.getAction().equals(UPDATE_WIDGET)){
            int[] widgetId = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, ImageMovieWidget.class));
            for (int id : widgetId){
                AppWidgetManager.getInstance(context).notifyAppWidgetViewDataChanged(id, R.id.imageView);
            }
        }

//        ComponentName thisWidget = new ComponentName(context, StackWidgetService.class);
//        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
//        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.stack_view);
        super.onReceive(context, intent);
    }
}

