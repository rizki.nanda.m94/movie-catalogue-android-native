package com.example.myappsubmission3;

import android.database.Cursor;

import com.example.myappsubmission3.db.DatabaseContract;

import static android.provider.BaseColumns._ID;
import static com.example.myappsubmission3.db.DatabaseContract.getColumnInt;
import static com.example.myappsubmission3.db.DatabaseContract.getColumnString;

public class ResItems {

    private int id;
    private String photo;
    private String name;
    private String description;

    public ResItems(Cursor cursor) {
        this.id = getColumnInt(cursor, _ID);
        this.name = getColumnString(cursor, DatabaseContract.MovieColumns.TITLE);
        this.photo = getColumnString(cursor, DatabaseContract.MovieColumns.PHOTO);
        this.description = getColumnString(cursor, DatabaseContract.MovieColumns.DESCRIPTION);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
