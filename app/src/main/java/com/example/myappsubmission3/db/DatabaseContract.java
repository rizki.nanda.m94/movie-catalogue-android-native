package com.example.myappsubmission3.db;

import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseContract {
    public static String TABLE_NAME = "movie";
    public static String TABLE_NAME_OF_TVSHOW = "tvshow";

    public static final String AUTHORITY = "com.example.myappsubmission3.db";

    public static final Uri CONTENT_URI = new Uri.Builder().scheme("content")
            .authority(AUTHORITY)
            .appendPath(TABLE_NAME)
            .build();

    public static final class MovieColumns implements BaseColumns {

        public static String PHOTO = "photo";
        public static String TITLE = "title";
        public static String DESCRIPTION = "description";
        public static String ORIGINAL_LANGUAGE = "original_language";
        public static String POPULARITY = "popularity";
        public static String RATING = "rating";
        public static String RELEASE_DATE = "release_date";
    }

    public static final class TvShowColumns implements BaseColumns {

        public static String PHOTO = "photo";
        public static String TITLE = "title";
        public static String DESCRIPTION = "description";
        public static String ORIGINAL_LANGUAGE = "original_language";
        public static String POPULARITY = "popularity";
        public static String RATING = "rating";
        public static String RELEASE_DATE = "release_date";
    }

    public static String getColumnString(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    public static int getColumnInt(Cursor cursor, String columnName) {
        return cursor.getInt(cursor.getColumnIndex(columnName));
    }
}
