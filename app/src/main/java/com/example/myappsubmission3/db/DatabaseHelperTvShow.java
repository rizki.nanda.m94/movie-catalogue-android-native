package com.example.myappsubmission3.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelperTvShow extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "dbtvshowfavorite";

    private static final int DATABASE_VERSION = 1;

    public DatabaseHelperTvShow(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private static final String SQL_CREATE_TABlE_TVSHOW= String.format("CREATE TABLE %s" +
                    " (%s INTEGER PRIMARY KEY," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL)",
            DatabaseContract.TABLE_NAME_OF_TVSHOW,
            DatabaseContract.TvShowColumns._ID,
            DatabaseContract.TvShowColumns.PHOTO,
            DatabaseContract.TvShowColumns.TITLE,
            DatabaseContract.TvShowColumns.DESCRIPTION,
            DatabaseContract.TvShowColumns.ORIGINAL_LANGUAGE,
            DatabaseContract.TvShowColumns.POPULARITY,
            DatabaseContract.TvShowColumns.RATING,
            DatabaseContract.TvShowColumns.RELEASE_DATE
    );

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABlE_TVSHOW);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.TABLE_NAME_OF_TVSHOW);
        onCreate(db);
    }
}
