package com.example.myappsubmission3;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.myappsubmission3.db.TvShowHelper;
import com.example.myappsubmission3.model.TvShowItems;
import com.squareup.picasso.Picasso;

import static android.provider.BaseColumns._ID;
import static com.example.myappsubmission3.db.DatabaseContract.TvShowColumns.DESCRIPTION;
import static com.example.myappsubmission3.db.DatabaseContract.TvShowColumns.ORIGINAL_LANGUAGE;
import static com.example.myappsubmission3.db.DatabaseContract.TvShowColumns.PHOTO;
import static com.example.myappsubmission3.db.DatabaseContract.TvShowColumns.POPULARITY;
import static com.example.myappsubmission3.db.DatabaseContract.TvShowColumns.RATING;
import static com.example.myappsubmission3.db.DatabaseContract.TvShowColumns.RELEASE_DATE;
import static com.example.myappsubmission3.db.DatabaseContract.TvShowColumns.TITLE;

public class TvShowDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private TvShowItems tv;
    private int position;

    private TvShowHelper tvShowHelper;

    public static final String EXTRA_IMG = "extra_img";
    public static final String EXTRA_TV = "extra_tvshow";

    public static final String EXTRA_POSITION = "extra_position";
    public static final int RESULT_ADD = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_show_detail);

        Toolbar toolbar = findViewById(R.id.toolbar_detail);
        TextView tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        ImageButton favButton = findViewById(R.id.btn_toolbar_profile);

        setSupportActionBar(toolbar);
        tv_toolbar_title.setText(R.string.information_detail);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        ImageView ivPhoto = findViewById(R.id.iv_image);
        TextView tvTitle = findViewById(R.id.text_title);
        TextView tvDescription = findViewById(R.id.text_overview);
        TextView tvOriginalLanguage = findViewById(R.id.text_language);
        TextView tvRelease= findViewById(R.id.text_release_date);
        TextView tvPopularity = findViewById(R.id.text_popularity);
        TextView tvRating = findViewById(R.id.text_rating);

        tvShowHelper = TvShowHelper.getInstance(getApplicationContext());
        tv = getIntent().getParcelableExtra(EXTRA_TV);

        String photo = tv.getPhoto();
        String title = tv.getName();
        String description = tv.getDescription();
        String release = tv.getRelease_date();
        String language = tv.getOriginal_language();
        String popularity = tv.getPopularity();
        String rating = tv.getRating();

        String photo_url_str = "https://image.tmdb.org/t/p/w342/"+photo;

        Picasso.get().load(photo_url_str).into(ivPhoto);

        tvTitle.setText(title);
        tvDescription.setText(description);
        tvOriginalLanguage.setText(language);
        tvPopularity.setText(popularity);
        tvRelease.setText(release);
        tvRating.setText(rating);

        favButton.setOnClickListener(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_toolbar_profile){
            int id = tv.getId();
            String photo = tv.getPhoto();
            String title = tv.getName();
            String description = tv.getDescription();
            String release = tv.getRelease_date();
            String language = tv.getOriginal_language();
            String popularity = tv.getPopularity();
            String rating = tv.getRating();

            tv.setName(title);
            tv.setDescription(description);
            tv.setRelease_date(release);
            tv.setOriginal_language(language);
            tv.setPopularity(popularity);
            tv.setRating(rating);
            tv.setPhoto(photo);

            Intent intent = new Intent();
            intent.putExtra(EXTRA_TV, tv);
            intent.putExtra(EXTRA_POSITION, position);

            ContentValues values = new ContentValues();
            values.put(_ID, id);
            values.put(PHOTO, photo);
            values.put(TITLE, title);
            values.put(DESCRIPTION, description);
            values.put(ORIGINAL_LANGUAGE, language);
            values.put(POPULARITY, popularity);
            values.put(RATING, rating);
            values.put(RELEASE_DATE, release);

            tvShowHelper.open();

            long result = tvShowHelper.insert(values);
            if(result > 0){
                tv.setId((int) result);
                setResult(RESULT_ADD, intent);
                finish();
                Toast.makeText(TvShowDetailActivity.this, "Berhasil menambahkan ke favorit", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(TvShowDetailActivity.this, "Tv Show sudah ditambahkan ke favorit", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
