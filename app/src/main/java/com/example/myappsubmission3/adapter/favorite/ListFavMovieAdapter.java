package com.example.myappsubmission3.adapter.favorite;

import android.app.Activity;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myappsubmission3.CustomOnItemClickListener;
import com.example.myappsubmission3.R;
import com.example.myappsubmission3.db.MovieHelper;
import com.example.myappsubmission3.model.MovieItems;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ListFavMovieAdapter extends RecyclerView.Adapter<ListFavMovieAdapter.FavMovieHolder> {

    private MovieItems movie;
    private MovieHelper movieHelper;

    private ListFavMovieAdapter.OnItemClickCallback onItemClickCallback;

    public void setOnItemClickCallback(ListFavMovieAdapter.OnItemClickCallback onItemClickCallback){
        this.onItemClickCallback = onItemClickCallback;
    }

    @NonNull
    @Override
    public FavMovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_movie_favorit, parent, false);
        return new FavMovieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FavMovieHolder holder, final int position) {
        String photo_url_str = "https://image.tmdb.org/t/p/w342/"+listMovie.get(position).getPhoto();
        Picasso.get().load(photo_url_str).into(holder.imgPhoto);
        holder.tvTitle.setText(listMovie.get(position).getName());
        holder.tvDescription.setText(listMovie.get(position).getDescription());

        holder.bind(listMovie.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onItemClicked(listMovie.get(holder.getAdapterPosition()));
            }
        });

        holder.btnDelete.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(final View view, final int position) {
                String dialogMessage, dialogTitle;

                movie = listMovie.get(position);
                movieHelper = MovieHelper.getInstance(view.getContext());

                dialogMessage = "Apakah anda yakin ingin menghapus movie ini dari daftar favorit ?";
                dialogTitle = "Hapus Movie";

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getContext());
                alertDialogBuilder.setTitle(dialogTitle);
                alertDialogBuilder
                        .setMessage(dialogMessage)
                        .setCancelable(false)
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int id) {
                                long result = movieHelper.deleteById(String.valueOf(movie.getId()));
                                if(result > 0){
                                    removeItem(position);
                                    Toast.makeText(view.getContext(), "Movie berhasil dihapus dari daftar favorit", Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(view.getContext(), "Gagal menghapus data", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }

            @Override
            public void onClick(View v) {

            }

        }));
    }

    @Override
    public int getItemCount() {
        return listMovie.size();
    }

    public interface OnItemClickCallback {
        void onItemClicked(MovieItems data);
    }

    public class FavMovieHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvDescription;
        ImageView imgPhoto;
        ImageButton btnDelete;

        public FavMovieHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.img_item_photo);
            tvTitle = itemView.findViewById(R.id.tv_item_name);
            tvDescription = itemView.findViewById(R.id.tv_item_detail);
            btnDelete = itemView.findViewById(R.id.imgButton_delete_movie);
        }

        public void bind(MovieItems movie) {
            String photo_url_str = "https://image.tmdb.org/t/p/w342/"+movie.getPhoto();

            Picasso.get().load(photo_url_str).into(imgPhoto);

            tvTitle.setText(movie.getName());
            tvDescription.setText(movie.getDescription());
        }
    }

    private ArrayList<MovieItems> listMovie = new ArrayList<>();
    private Activity activity;

    public ListFavMovieAdapter(FragmentActivity activity){
        this.activity = activity;
    }

    public void setListMovie(ArrayList<MovieItems> listMovie){
        if(listMovie.size() > 0){
            this.listMovie.clear();
        }

        this.listMovie.addAll(listMovie);

        notifyDataSetChanged();
    }

    public void removeItem(int position){
        this.listMovie.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, listMovie.size());
    }

}
