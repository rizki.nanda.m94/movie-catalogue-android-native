package com.example.myappsubmission3.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myappsubmission3.R;
import com.example.myappsubmission3.model.TvShowItems;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListTvShowAdapter extends RecyclerView.Adapter<ListTvShowAdapter.MyViewHolder> {

    ArrayList<TvShowItems> mData = new ArrayList<>();

    private ListTvShowAdapter.OnItemClickCallback onItemClickCallback;

    public void setData(ArrayList<TvShowItems> items){
        mData.clear();
        mData.addAll(items);
        notifyDataSetChanged();
    }

    public void setOnItemClickCallback(ListTvShowAdapter.OnItemClickCallback onItemClickCallback){
        this.onItemClickCallback = onItemClickCallback;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v;
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_tv_show, viewGroup, false);
        MyViewHolder vHolder = new MyViewHolder(v);

        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        holder.bind(mData.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onItemClicked(mData.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imgPhoto;
        TextView tvName, tvDescription;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.img_item_photo);
            tvName = itemView.findViewById(R.id.tv_item_name);
            tvDescription = itemView.findViewById(R.id.tv_item_detail);
        }

        public void bind(TvShowItems tvShow) {

            String photo_url_str = "https://image.tmdb.org/t/p/w342/"+tvShow.getPhoto();

            Picasso.get().load(photo_url_str).into(imgPhoto);

            tvName.setText(tvShow.getName());
            tvDescription.setText(tvShow.getDescription());
        }
    }

    public interface OnItemClickCallback {
        void onItemClicked(TvShowItems data);
    }
}
