package com.example.myappsubmission3.adapter.favorite;

import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myappsubmission3.CustomOnItemClickListener;
import com.example.myappsubmission3.R;
import com.example.myappsubmission3.db.TvShowHelper;
import com.example.myappsubmission3.model.TvShowItems;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListFavTvShowAdapter extends RecyclerView.Adapter<ListFavTvShowAdapter.FavTvShowHolder> {

    private TvShowItems tv;
    private TvShowHelper tvShowHelper;

    private ArrayList<TvShowItems> listTv = new ArrayList<>();
    private AppCompatActivity activity;
    private OnItemClickCallback onItemClickCallback;

    public ListFavTvShowAdapter(FragmentActivity activity) {
        this.activity = (AppCompatActivity) activity;
    }

    public void setOnItemClickCallback(ListFavTvShowAdapter.OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    @NonNull
    @Override
    public FavTvShowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_tv_show_favorit, parent, false);
        return new FavTvShowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FavTvShowHolder holder, int position) {
        String photo_url_str = "https://image.tmdb.org/t/p/w342/" + listTv.get(position).getPhoto();
        Picasso.get().load(photo_url_str).into(holder.imgPhoto);
        holder.tvTitle.setText(listTv.get(position).getName());
        holder.tvDescription.setText(listTv.get(position).getDescription());

        holder.bind(listTv.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onItemClicked(listTv.get(holder.getAdapterPosition()));
            }
        });

        holder.btnDelete.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {

            @Override
            public void onItemClicked(final View view, final int position) {
                String dialogMessage, dialogTitle;

                tv = listTv.get(position);
                tvShowHelper = TvShowHelper.getInstance(view.getContext());

                dialogMessage = "Apakah anda yakin ingin menghapus acara ini dari daftar favorit ?";
                dialogTitle = "Hapus TvShow";

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getContext());
                alertDialogBuilder.setTitle(dialogTitle);
                alertDialogBuilder
                        .setMessage(dialogMessage)
                        .setCancelable(false)
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int id) {
                                long result = tvShowHelper.deleteById(String.valueOf(tv.getId()));
                                if (result > 0) {
                                    removeItem(position);
                                    Toast.makeText(view.getContext(), "TvShow berhasil dihapus dari daftar favorit", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(view.getContext(), "Gagal menghapus data", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }

            @Override
            public void onClick(View v) {

            }
        }));
    }

    @Override
    public int getItemCount() {
        return listTv.size();
    }

    public interface OnItemClickCallback {
        void onItemClicked(TvShowItems data);
    }

    public class FavTvShowHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvDescription;
        ImageView imgPhoto;
        ImageButton btnDelete;

        public FavTvShowHolder(@NonNull View itemView) {
            super(itemView);

            imgPhoto = itemView.findViewById(R.id.img_item_photo);
            tvTitle = itemView.findViewById(R.id.tv_item_name);
            tvDescription = itemView.findViewById(R.id.tv_item_detail);
            btnDelete = itemView.findViewById(R.id.imgButton_delete_tvshow);
        }

        public void bind(TvShowItems tvShow) {
            String photo_url_str = "https://image.tmdb.org/t/p/w342/" + tvShow.getPhoto();

            Picasso.get().load(photo_url_str).into(imgPhoto);

            tvTitle.setText(tvShow.getName());
            tvDescription.setText(tvShow.getDescription());
        }
    }

    public void setListTvShow(ArrayList<TvShowItems> tv) {
        if (tv.size() > 0) {
            this.listTv.clear();
        }

        this.listTv.addAll(tv);

        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        this.listTv.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, listTv.size());
    }
}
