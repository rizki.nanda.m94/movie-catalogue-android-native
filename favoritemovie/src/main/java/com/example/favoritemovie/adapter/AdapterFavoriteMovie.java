package com.example.favoritemovie.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.favoritemovie.R;
import com.example.favoritemovie.db.DatabaseContract;
import com.squareup.picasso.Picasso;

import static com.example.favoritemovie.db.DatabaseContract.getColumnString;

public class AdapterFavoriteMovie extends CursorAdapter {

    public AdapterFavoriteMovie(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_favorit_list, parent, false);
        return v;
    }

//    @Override
    public Cursor getCusor(){
        return super.getCursor();
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        if(cursor != null){
            ImageView imgFilm = (ImageView) view.findViewById(R.id.img_item_photo);
            TextView tvNamaFilm = (TextView) view.findViewById(R.id.tv_item_name);
            TextView tvDescFilm = (TextView) view.findViewById(R.id.tv_item_detail);

            String photo_url_str = "https://image.tmdb.org/t/p/w342/"+getColumnString(cursor,  DatabaseContract.MovieColumns.PHOTO);
            Picasso.get().load(photo_url_str).into(imgFilm);
            tvNamaFilm.setText(getColumnString(cursor, DatabaseContract.MovieColumns.TITLE));
            tvDescFilm.setText(getColumnString(cursor, DatabaseContract.MovieColumns.DESCRIPTION));
        }
    }
}
